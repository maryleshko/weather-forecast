import Foundation

class NetworkManager {
    
    static var shared: NetworkManager = NetworkManager()
    
    private init(){}
    
    func openWeatherJSON(lat: Double, lon: Double, completion: @escaping (_ weather: ModelForPresenting) -> ()) {
        let baseURL = "https://api.openweathermap.org/data/2.5/forecast?lat="
        let urlString = baseURL + "\(lat)&lon=\(lon)&appid=cd339fd2cc0f489fd08b49ae5bce3e45&units=metric"
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                let data = data else { return }
            
            do {

                let object = try JSONDecoder().decode(WeatherDescription.self, from: data)

                let weather = NetworkManager.shared.createWeatherObject(from: object)

                let weatherForPresenting = NetworkManager.shared.presentingWeather(fromWeather: weather)
                completion(weatherForPresenting)

                print()
                
            } catch let error {
                print(error)
            }
        }
        task.resume()
    }

    func openWeatherJSONHourly(lat: Double, lon: Double, completion: @escaping (_ weather: [WeekdaysArrayModel], WeatherInfo) -> ()) {
        let baseURL = "https://api.openweathermap.org/data/2.5/forecast?lat="
        let urlString = baseURL + "\(lat)&lon=\(lon)&appid=cd339fd2cc0f489fd08b49ae5bce3e45&units=metric"
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                let data = data else { return }

            do {

                var object = try JSONDecoder().decode(WeatherDescription.self, from: data)
                let weather = NetworkManager.shared.createWeatherObject(from: object)
                object.list?.removeFirst()

                let hourlyWeather = NetworkManager.shared.createHourlyWeatherArray(from: object)

                let weatherArray = NetworkManager.shared.changingDataToDayToHourArray(fromData: hourlyWeather)
                completion(weatherArray, weather)

                print()

            } catch let error {
                print(error)
            }
        }
        task.resume()
    }
    
    func cityJSON(completion: @escaping (_ cities: [CityModel]) -> ()) {
        if let path = Bundle.main.path(forResource: "city.list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print()
                var array = [CityModel]()
                var sortedArray = [CityModel]()
                if jsonResult is [Any] {
                    let cityList = try JSONDecoder().decode([CityModel].self, from: data)
                    for element in cityList {
                        let city = CityModel(id: element.id, name: element.name, state: element.state, country: element.country, coord: element.coord)
                        array.append(city)
                    }
                    sortedArray = array.sorted (by: {$0.name < $1.name})
                    completion(sortedArray)
                    CitiesManager.shared.setArray(sortedArray)
                }
            } catch {
            }
        }
    }

    func createWeatherObject(from object: WeatherDescription) -> WeatherInfo {
        let weather = WeatherInfo(name: object.city?.name, country: object.city?.country, temp: object.list?[0].main?.temp, descr: object.list?[0].weather?[0].main, rain: object.list?[0].clouds?.all, rainVolume: object.list?[0].rain?.threeHours, pressure: object.list?[0].main?.pressure, windSpeed: object.list?[0].wind?.speed, windDirection: object.list?[0].wind?.degree, icon: object.list?[0].weather?[0].icon, day: object.list?[0].dt?.unixToDate().dateToDate())
        return weather
    }

    func createHourlyWeatherArray(from object: WeatherDescription) -> [ThreeHourWeather] {
        var hourlyWeather = [ThreeHourWeather()]
        for element in object.list ?? [] {
            let hourWeather = ThreeHourWeather(temp: element.main?.temp, time: element.dt?.unixToDate().dateToHour(), descr: element.weather?[0].main, icon: element.weather?[0].icon, date: element.dt?.unixToDate(), stringDate: element.dt?.unixToDate().dateToDate())
            hourlyWeather.append(hourWeather)
        }
        hourlyWeather.removeFirst()
        return hourlyWeather
    }

    func changingDataToDayToHourArray(fromData hourlyWeather: [ThreeHourWeather]) -> [WeekdaysArrayModel] {
        let uniqueDays = hourlyWeather.map { $0.stringDate }
        var newDays = [String]()
        for element in uniqueDays {
            guard let day = element else {return []}
            if newDays.contains(day) == false {
                newDays.append(day)
            }
        }

        var weatherArray = [WeekdaysArrayModel]()

        let dict = Dictionary(grouping: hourlyWeather) { (model) -> String in
            return model.stringDate ?? ""
        }

        for day in newDays {
            let array = WeekdaysArrayModel()
            array.weekDay = day
            array.weather = []
            weatherArray.append(array)
        }

        for element in weatherArray {
            element.weather = dict[element.weekDay ?? ""]
        }

        return weatherArray
    }
    func presentingWeather(fromWeather weather: WeatherInfo) -> ModelForPresenting {
        let model = ModelForPresenting(probability: weather.rain, rainVolume: weather.rainVolume, pressure: weather.pressure, wind_speed: weather.windSpeed, windDir: weather.windDirection, name: weather.name, country: weather.country, temp: weather.temp, descr: weather.descr, icon: weather.icon)
        return model
    }
    
}

extension Int {
    func unixToDate() -> Date {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        return date
    }
}
extension Date {
    func dateToHour() -> String {
        let formatter = DateFormatter()
        let timeZone = NSTimeZone(name:"UTC")
        formatter.timeZone = timeZone as TimeZone?
        formatter.calendar = Calendar.current
        formatter.dateFormat = "HH:mm"

        return formatter.string(from: self)
    }
    func dateToDate() -> String {
        let formatter = DateFormatter()
        let timeZone = NSTimeZone(name:"UTC")
        formatter.timeZone = timeZone as TimeZone?
        formatter.calendar = Calendar.current
        formatter.dateFormat = "EEEE"

        return formatter.string(from: self)
    }
}
