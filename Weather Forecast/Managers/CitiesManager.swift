import Foundation

class CitiesManager {
    static let shared = CitiesManager()
    
    let key = "cities"
    
    func getArray() -> [CityModel] {
        if let cityArray = UserDefaults.standard.value([CityModel].self, forKey: key) {
            return cityArray
        }
        return []
    }
    func setArray(_ cities: [CityModel]) {
        UserDefaults.standard.set(encodable: cities, forKey: key)
    }
}
extension UserDefaults {

    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }

    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
