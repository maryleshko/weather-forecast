import Foundation
import CoreLocation

protocol LocationManagerProtocol: AnyObject {
    func setValue()
}

class LocationManager: NSObject, CLLocationManagerDelegate {

    static let shared = LocationManager()
    var coordinates : CLLocationCoordinate2D

    var manager : CLLocationManager
    weak var delegate: LocationManagerProtocol?


    override init() {
        manager = CLLocationManager()
        coordinates = CLLocationCoordinate2D()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = 1000

        super.init()
        manager.delegate = self
    }

    func start() {
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.coordinates = location.coordinate
        self.manager.stopUpdatingLocation()
        print(self.coordinates)
        self.delegate?.setValue()

    }
}
