import Foundation

class WeekdaysArrayModel {
    var weekDay: String?
    var weather: [ThreeHourWeather]?
    
    init() {}
    
    init(weekDay: String?, weather: [ThreeHourWeather]?) {
        self.weekDay = weekDay
        self.weather = weather
    }
}
