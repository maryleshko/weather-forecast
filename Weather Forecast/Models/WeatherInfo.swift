import Foundation

struct WeatherInfo {
    var name: String?
    var country: String?
    var temp: Double?
    var descr: String?
    var rain: Int?
    var rainVolume: Double?
    var pressure: Int?
    var windSpeed: Double?
    var windDirection: Int?
    var icon: String?
    var day: String?
    
    init() {}
    
    init(name: String?, country: String?, temp: Double?, descr: String?, rain: Int?, rainVolume: Double?, pressure: Int?, windSpeed: Double?, windDirection: Int?, icon: String?, day: String?) {
        self.name = name
        self.country = country
        self.temp = temp
        self.descr = descr
        self.rain = rain
        self.rainVolume = rainVolume
        self.pressure = pressure
        self.windSpeed = windSpeed
        self.windDirection = windDirection
        self.icon = icon
        self.day = day
    }

    
}
