import Foundation

struct WeatherDescription: Codable {
    var cod: String?
    var message: Int?
    var cnt: Int?
    var list: [HourlyWeather]?
    var city: City?

    init() {}
    
    enum CodingKeys: String, CodingKey {
        case cod, message, cnt, list, city
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cod, forKey: .cod)
        try container.encode(message, forKey: .message)
        try container.encode(cnt, forKey: .cnt)
        try container.encode(list, forKey: .list)
        try container.encode(city, forKey: .city)
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        cod = try container.decodeIfPresent(String.self, forKey: .cod)
        message = try container.decodeIfPresent(Int.self, forKey: .message)
        cnt = try container.decodeIfPresent(Int.self, forKey: .cnt)
        list = try container.decodeIfPresent([HourlyWeather].self, forKey: .list)
        city = try container.decodeIfPresent(City.self, forKey: .city)
    }
}

struct HourlyWeather: Codable {
    var dt: Int?
    var main: Main?
    var weather: [Weather]?
    var clouds: Clouds?
    var wind: Wind?
    var visibility: Int?
    var pop: Double?
    var rain: Rain?
    var sys: Sys?
    var dtTxt: Int?

    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, visibility, pop, rain, sys, dtTxt
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(dt, forKey: .dt)
        try container.encode(main, forKey: .main)
        try container.encode(weather, forKey: .weather)
        try container.encode(clouds, forKey: .clouds)
        try container.encode(wind, forKey: .wind)
        try container.encode(visibility, forKey: .visibility)
        try container.encode(pop, forKey: .pop)
        try container.encode(sys, forKey: .sys)
        try container.encode(dtTxt, forKey: .dtTxt)
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dt = try container.decodeIfPresent(Int.self, forKey: .dt)
        main = try container.decodeIfPresent(Main.self, forKey: .main)
        weather = try container.decode([Weather].self, forKey: .weather)
        clouds = try container.decodeIfPresent(Clouds.self, forKey: .clouds)
        wind = try container.decodeIfPresent(Wind.self, forKey: .wind)
        visibility = try container.decodeIfPresent(Int.self, forKey: .visibility)
        pop = try container.decodeIfPresent(Double.self, forKey: .pop)
        rain = try container.decodeIfPresent(Rain.self, forKey: .rain)
        sys = try container.decodeIfPresent(Sys.self, forKey: .sys)
        dtTxt = try container.decodeIfPresent(Int.self, forKey: .dtTxt)
    }

}

struct Main: Codable {
    var temp: Double?
    var feels_like: Double?
    var tempMin: Double?
    var tempMax: Double?
    var pressure: Int?
    var seaLevel: Int?
    var grndLevel: Int?
    var humidity: Int?
    var tempKf: Double?

    enum CodingKeys: String, CodingKey {
        case temp, feels_like, tempMin, tempMax, pressure, seaLevel, grndLevel, humidity, tempKf
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(temp, forKey: .temp)
        try container.encode(feels_like, forKey: .feels_like)
        try container.encode(tempMin, forKey: .tempMin)
        try container.encode(tempMax, forKey: .tempMax)
        try container.encode(pressure, forKey: .pressure)
        try container.encode(seaLevel, forKey: .seaLevel)
        try container.encode(grndLevel, forKey: .grndLevel)
        try container.encode(humidity, forKey: .humidity)
        try container.encode(tempKf, forKey: .tempKf)
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        temp = try container.decodeIfPresent(Double.self, forKey: .temp)
        feels_like = try container.decodeIfPresent(Double.self, forKey: .feels_like)
        tempMin = try container.decodeIfPresent(Double.self, forKey: .tempMin)
        tempMax = try container.decodeIfPresent(Double.self, forKey: .tempMax)
        pressure = try container.decodeIfPresent(Int.self, forKey: .pressure)
        seaLevel = try container.decodeIfPresent(Int.self, forKey: .seaLevel)
        grndLevel = try container.decodeIfPresent(Int.self, forKey: .grndLevel)
        humidity = try container.decodeIfPresent(Int.self, forKey: .humidity)
        tempKf = try container.decodeIfPresent(Double.self, forKey: .tempKf)
    }

}

struct City: Codable {
    var id: Int?
    var name: String?
    var coord: Coordinates?
    var country: String?
    var timezone: Int?
    var sunrise: Int?
    var sunset: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, coord, country, timezone, sunrise, sunset
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(coord, forKey: .coord)
        try container.encode(country, forKey: .country)
        try container.encode(timezone, forKey: .timezone)
        try container.encode(sunrise, forKey: .sunrise)
        try container.encode(sunset, forKey: .sunset)

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        coord = try container.decodeIfPresent(Coordinates.self, forKey: .coord)
        country = try container.decodeIfPresent(String.self, forKey: .country)
        timezone = try container.decodeIfPresent(Int.self, forKey: .timezone)
        sunrise = try container.decodeIfPresent(Int.self, forKey: .sunrise)
        sunset = try container.decodeIfPresent(Int.self, forKey: .sunset)

    }
}

struct Coordinates: Codable {
    var lat: Double?
    var lon: Double?

    init() {}

    init(lat: Double?, lon: Double?) {
        self.lat = lat
        self.lon = lon
    }

    public enum CodingKeys: String, CodingKey {
        case lat, lon
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat)
        self.lon = try container.decodeIfPresent(Double.self, forKey: .lon)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.lat, forKey: .lat)
        try container.encode(self.lon, forKey: .lon)
    }
}

struct Weather: Codable {
    var id: Int?
    var main: String?
    var describtion: String?
    var icon: String?

    enum CodingKeys: String, CodingKey {
        case id, main, describtion, icon
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(main, forKey: .main)
        try container.encode(describtion, forKey: .describtion)
        try container.encode(icon, forKey: .icon)

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        main = try container.decodeIfPresent(String.self, forKey: .main)
        describtion = try container.decodeIfPresent(String.self, forKey: .describtion)
        icon = try container.decodeIfPresent(String.self, forKey: .icon)
    }
}

struct Clouds: Codable {
    var all: Int?

    enum CodingKeys: String, CodingKey {
        case all
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(all, forKey: .all)

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        all = try container.decodeIfPresent(Int.self, forKey: .all)

    }
}

struct Wind: Codable {
    var speed: Double?
    var degree: Int?

    enum CodingKeys: String, CodingKey {
        case speed, degree
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(speed, forKey: .speed)
        try container.encode(degree, forKey: .degree)

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        speed = try container.decodeIfPresent(Double.self, forKey: .speed)
        degree = try container.decodeIfPresent(Int.self, forKey: .degree)
    }
}

struct Rain: Codable {
    var threeHours: Double?
    enum CodingKeys: String, CodingKey {
        case threeHours = "3h"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(threeHours, forKey: .threeHours)

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        threeHours = try container.decodeIfPresent(Double.self, forKey: .threeHours)
    }
}

struct Sys: Codable {
    var pod: String?

    enum CodingKeys: String, CodingKey {
        case pod
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(pod, forKey: .pod)

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        pod = try container.decodeIfPresent(String.self, forKey: .pod)

    }
}
