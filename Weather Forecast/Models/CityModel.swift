import Foundation

struct CityModel: Codable {
    var id: Int?
    var name: String = ""
    var state: String?
    var country: String?
    var coord : Coordinates?
    
    init() {}
     
    init(id: Int?, name: String, state: String?, country: String?, coord: Coordinates?) {
        self.id = id
        self.name = name
        self.state = state
        self.country = country
        self.coord = coord
     }
     
     public enum CodingKeys: String, CodingKey {
         case id, name, state, country, coord
     }
    
     init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try (container.decodeIfPresent(String.self, forKey: .name) ?? "")
        self.state = try container.decodeIfPresent(String.self, forKey: .state)
        self.country = try container.decodeIfPresent(String.self, forKey: .country)
        self.coord = try container.decodeIfPresent(Coordinates.self, forKey: .coord)
        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(self.id, forKey: .id)
            try container.encode(self.name, forKey: .name)
            try container.encode(self.state, forKey: .state)
            try container.encode(self.country, forKey: .country)
            try container.encode(self.coord, forKey: .coord)
        }

}
