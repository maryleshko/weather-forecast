import Foundation

class ThreeHourWeather: Codable {
    var temp: Double?
    var time: String?
    var descr: String?
    var icon: String?
    var date: Date?
    var stringDate: String?
    
    init() {}

    init(temp: Double?, time: String?, descr: String?, icon: String?, date: Date?, stringDate: String?) {
        self.temp = temp
        self.time = time
        self.descr = descr
        self.icon = icon
        self.date = date
        self.stringDate = stringDate
    }

    public enum CodingKeys: String, CodingKey {
         case temp, time, descr, icon, date, stringDate
     }

    required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
        self.temp = try container.decodeIfPresent(Double.self, forKey: .temp)
        
        self.time = try container.decodeIfPresent(String.self, forKey: .time)
        self.descr = try container.decodeIfPresent(String.self, forKey: .descr)
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        self.date = try container.decodeIfPresent(Date.self, forKey: .date)
        self.stringDate = try container.decodeIfPresent(String.self, forKey: .stringDate)

        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(self.temp, forKey: .temp)
            try container.encode(self.time, forKey: .time)
            try container.encode(self.descr, forKey: .descr)
            try container.encode(self.icon, forKey: .icon)
            try container.encode(self.date, forKey: .date)
            try container.encode(self.stringDate, forKey: .stringDate)

        }

}
