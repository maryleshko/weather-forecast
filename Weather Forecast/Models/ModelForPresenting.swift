import Foundation

struct ModelForPresenting {
    var probability: String?
    var rainVolume: String?
    var pressure: String?
    var wind_speed: String?
    var windDir: String?
    var city: String?
    var temp: String?
    var icon: String?
    var textForSharing: String?

    init() {}

    init(probability: Int?, rainVolume: Double?, pressure: Int?, wind_speed: Double?, windDir: Int?, name: String?, country: String?, temp: Double?, descr: String?, icon: String?) {
        self.probability = "\(String(describing: probability ?? 0)) %"
        self.rainVolume = "\(String(describing: rainVolume ?? 0.0)) mm"
        self.pressure = "\(pressure ?? 0) hPa"
        self.wind_speed = "\(wind_speed ?? 0) km/h"
        let windDeg = windDir ?? 0
        if windDeg > 0, windDeg < 90 {
            self.windDir = "N"
        } else if windDeg >= 90, windDeg < 180 {
            self.windDir = "E"
        } else if windDeg >= 180, windDeg < 270 {
            self.windDir = "S"
        } else {
            self.windDir = "W"
        }
        self.city = "\(name ?? ""), \(country ?? "")"
        self.temp = "\(String(Int(temp?.rounded() ?? 0.0))) ° | \(descr?.uppercased() ?? "")"
        self.icon = icon ?? ""
        self.textForSharing = "You're located in \(name ?? ""). The weather is \(descr?.lowercased() ?? ""), \(String(Int(temp?.rounded() ?? 0.0))) °."
    }
}
