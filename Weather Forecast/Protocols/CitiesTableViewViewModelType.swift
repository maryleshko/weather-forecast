import Foundation

protocol CitiesTableViewViewModelType {
    var citiesArray: [CityModel] { get }
    var searchResults: [CityModel] { get }
    func numberOfRowsInSection(_ isFiltering: Bool) -> Int
    func filterContent(_ searchText: String)
    func setCell(_ indexPath: IndexPath, _ isFiltering: Bool) -> CityModel

    
}
