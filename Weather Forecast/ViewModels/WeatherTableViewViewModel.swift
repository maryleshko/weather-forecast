import Foundation
import  CoreLocation

class WeatherTableViewViewModel: LocationManagerProtocol {

    var weatherArrayForPresenting = Bindable<[WeekdaysArrayModel]>([WeekdaysArrayModel()])
    var currentWeather = Bindable<WeatherInfo>(WeatherInfo())

    var coordinates = Bindable<CLLocationCoordinate2D>(CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))

    func getCoordinates() {
        LocationManager.shared.start()
        LocationManager.shared.delegate = self

    }

    func setValue() {
            self.coordinates.value = LocationManager.shared.coordinates
            getWeather(lat: self.coordinates.value.latitude, lon: self.coordinates.value.longitude)

        }

    func getWeather(lat: Double, lon: Double) {
                    NetworkManager.shared.openWeatherJSONHourly(lat: lat, lon: lon) { (array, weather) in
                        self.weatherArrayForPresenting.value = array
                        self.currentWeather.value = weather
    }
    }
    func updateCity(lat: Double, lon: Double) {
        self.coordinates.value.latitude = lat
        self.coordinates.value.longitude = lon
        getWeather(lat: self.coordinates.value.latitude, lon: self.coordinates.value.longitude)

    }


}
