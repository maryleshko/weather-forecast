import Foundation
import CoreLocation

class CurrentWeatherViewControllerViewModel: LocationManagerProtocol {

    let kLocationDidChangeNotification = "LocationDidChangeNotification"

    var currentWeather = Bindable<WeatherInfo>(WeatherInfo())

    var weatherForPresenting = Bindable<ModelForPresenting>(ModelForPresenting())

    var coordinates = Bindable<CLLocationCoordinate2D>(CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))

    func getCoordinates() {

           LocationManager.shared.start()
           LocationManager.shared.delegate = self

       }

    func setValue() {
        self.coordinates.value = LocationManager.shared.coordinates
        getWeather(lat: self.coordinates.value.latitude, lon: self.coordinates.value.longitude)

    }

    func getFirstWeather(lat: Double, lon: Double) {
        let array = CitiesManager.shared.getArray()
        if array.count == 0 {
            NetworkManager.shared.cityJSON { (_) in
                NetworkManager.shared.openWeatherJSON(lat: lat, lon: lon) { (weather) in
                    self.weatherForPresenting.value = weather

                }
            }
        } else {
            NetworkManager.shared.openWeatherJSON(lat: lat, lon: lon) { (weather) in
                self.weatherForPresenting.value = weather
            }
        }
    }

    func getWeather(lat: Double, lon: Double) {
            NetworkManager.shared.openWeatherJSON(lat: lat, lon: lon) { (weather) in
                self.weatherForPresenting.value = weather
            }
    }

    func updateCity(lat: Double, lon: Double) {
        getWeather(lat: lat, lon: lon)

    }


}
