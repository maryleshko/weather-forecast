import Foundation

class CitiesTableViewViewModel: CitiesTableViewViewModelType {

    var object = Bindable<WeatherDescription>(WeatherDescription())

    var citiesArray = CitiesManager.shared.getArray()
    var searchResults = [CityModel]()
    
    func numberOfRowsInSection(_ isFiltering: Bool) -> Int {
        if isFiltering == true {
            return searchResults.count
        } else {
            return 0
        }
    }
    
    func filterContent(_ searchText: String) {
        searchResults = citiesArray.filter { (city: CityModel) -> Bool in
            return city.name.lowercased().contains(searchText.lowercased())
        }
    }
    
    func setCell(_ indexPath: IndexPath, _ isFiltering: Bool) -> CityModel {
        var city = CityModel()
        if isFiltering == true {
            city = searchResults[indexPath.row]
        }
        return city
    }
    
    func getJson(indexPath: IndexPath, isFiltering: Bool, completion: @escaping (_ weather: String) -> ()) {
        
        let city = setCell(indexPath, isFiltering)
        let lat = city.coord?.lat ?? 0.0
        let lon = city.coord?.lon ?? 0.0
        
            NetworkManager.shared.openWeatherJSON(lat: lat, lon: lon) { (_) in
                completion("OK")
        }
        }

    func getCoorsinates(indexPath: IndexPath, isFiltering: Bool) -> (Double, Double) {
        let city = setCell(indexPath, isFiltering)
        let lat = city.coord?.lat ?? 0.0
        let lon = city.coord?.lon ?? 0.0
        return (lat, lon)
    }
        
    }


