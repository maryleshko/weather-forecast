import Foundation
import UIKit
import PureLayout

class DescriptionFormView: UIView {
    
    init(label: UILabel, image: UIImageView) {
        super.init(frame: .zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        image.translatesAutoresizingMaskIntoConstraints = false
        image.autoSetDimensions(to: CGSize(width: 30, height: 30))

        self.addSubview(image)
        self.addSubview(label)

        image.autoAlignAxis(.vertical, toSameAxisOf: self)
        label.autoAlignAxis(.vertical, toSameAxisOf: self)

        image.autoPinEdge(.top, to: .top, of: self, withOffset: 5)
        label.autoPinEdge(.top, to: .bottom, of: image, withOffset: 0)
        label.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: 0)
        label.autoPinEdge(toSuperviewEdge: .left, withInset: 5)
        label.autoPinEdge(toSuperviewEdge: .right, withInset: 5)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
