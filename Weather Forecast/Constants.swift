import Foundation
import UIKit

class Constants {
    
    static let rowHeight: CGFloat = 100
    static let sectionHeight: CGFloat = 50
    static let todayItemImageName = "01d"
    static let forecastItemImageName = "10d"
    static let titleLabelConstraint: CGFloat = 50
    static let titleLabelHeight: CGFloat = 100
    static let titleLabelFont: CGFloat = 50
    static let todayItemTitle = "TODAY"
    static let forecastItemTitle = "FORECAST"
    static let font = "Gladish"
    static let plusSize: CGFloat = 15
    static let dottedImageName = "dotted-bar"
    static let probabilityImageName = "cloud"
    static let rainImageName = "wet"
    static let pressureImageName = "atmospheric-pressure"
    static let windSpeedImageName = "wind-speed"
    static let windDirectionImageName = "wind"
    static let shareButtonText = "SHARE"

}

