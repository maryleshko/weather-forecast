import UIKit
import PureLayout
import SnapKit

// MARK: - Protocols

protocol CurrentWeatherViewControllerProtocol: AnyObject {
    func updateCity(lat: Double, lon: Double)
}

class CurrentWeatherViewController: UIViewController {
    
    // MARK: - Private Properties


    private let cityLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let tempLabel : UILabel = {
        let label = UILabel()
        label.textColor = .systemBlue
        label.font = .systemFont(ofSize: 25.0)
        label.textAlignment = .center
        return label
    }()
    private let probabilityLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let rainLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let pressureLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let windSpeedLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let windDirectionLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let emptyLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let secondEmptyLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private let shareButton : UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(buttonShared(_:)), for: .touchUpInside)
        button.setTitle("SHARE", for: .normal)
        button.setTitleColor(.orange, for: .normal)

        return button
    }()
    private let weatherIcon : UIImageView = {
        let icon = UIImageView()
        icon.clipsToBounds = true
        return icon
    }()
    private let dottedLine : UIImageView = {
        let line = UIImageView()
        line.image = UIImage(named: Constants.dottedImageName)
        return line
    }()
    private let secondDottedLine : UIImageView = {
        let line = UIImageView()
        line.image = UIImage(named: Constants.dottedImageName)
        return line
    }()

    // MARK: - Public Properties

    var emptyImageView = UIImageView()
    var iconName = String()
    var probabilityImageView = UIImageView()
    var rainImageView = UIImageView()
    var pressureImageView = UIImageView()
    var windSpeedImageView = UIImageView()
    var windDirectionImageView = UIImageView()
    var secondEmptyImageView = UIImageView()
    var firstStackView = UIStackView()
    var secondStackView = UIStackView()
    var textForSharing = String()
    var tabBarHeight = CGFloat()
    var currentWeather = WeatherInfo()
    var viewModel: CurrentWeatherViewControllerViewModel?
    weak var delegate: CurrentWeatherViewControllerProtocol?

    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        NetworkConnection.checkConnection(sender: self)
        
        let rightBarButton = UIBarButtonItem(title: "CHOOSE", style: .done, target: self, action: #selector(openCitiesController(_:)))
        navigationItem.setRightBarButton(rightBarButton, animated: true)

        viewModel = CurrentWeatherViewControllerViewModel()
        viewModel?.getCoordinates()


        tabBarHeight = tabBarController?.tabBar.frame.size.height ?? 0
        addSubviews()
        setupConstraints()
        bindOutlets()

    }
    
    // MARK: - IBActions

    @IBAction func openCitiesController(_ sender: AnyObject) {
        let controller = CitiesTableViewController()
        controller.delegate = self
        controller.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonShared(_ sender: AnyObject) {
        let textShare = [ textForSharing ]
        let shareController = UIActivityViewController(activityItems: textShare, applicationActivities: nil)
        shareController.completionWithItemsHandler = {_, bool, _, _ in
            if bool {
                print("Success")
            }
        }
        self.present(shareController, animated: true, completion: nil)
    }
    
    // MARK: - Private functions

    private func bindOutlets() {
        self.viewModel?.weatherForPresenting.bind({ (value) in
            DispatchQueue.main.async {
            self.probabilityLabel.text = value.probability
            self.rainLabel.text = value.rainVolume
            self.pressureLabel.text = value.pressure
            self.windSpeedLabel.text = value.wind_speed
            self.windDirectionLabel.text = value.windDir
            self.cityLabel.text = value.city
            self.tempLabel.text = value.temp
            self.weatherIcon.image = UIImage(named: value.icon ?? "")
            self.textForSharing = value.textForSharing ?? ""
            }
        })
    }
    
    private func addSubviews() {
        view.addSubview(weatherIcon)
        view.addSubview(cityLabel)
        view.addSubview(tempLabel)
        view.addSubview(dottedLine)
        
        emptyImageView.image = UIImage(named: "")
        secondEmptyImageView.image = UIImage(named: "")

        probabilityImageView.image = UIImage(named: Constants.probabilityImageName)
        let probabilityView = DescriptionFormView(label: probabilityLabel, image: probabilityImageView)
        rainImageView.image = UIImage(named: Constants.rainImageName)
        let rainView = DescriptionFormView(label: rainLabel, image: rainImageView)
        pressureImageView.image = UIImage(named: Constants.pressureImageName)
        let pressureView = DescriptionFormView(label: pressureLabel, image: pressureImageView)

        firstStackView = UIStackView(arrangedSubviews: [probabilityView, rainView, pressureView])
        firstStackView.distribution = .fillEqually
        firstStackView.axis = .horizontal
        firstStackView.spacing = 0
        view.addSubview(firstStackView)

        let emptyView = DescriptionFormView(label: emptyLabel, image: emptyImageView)
        windSpeedImageView.image = UIImage(named: Constants.windSpeedImageName)
        let windSpeedView = DescriptionFormView(label: windSpeedLabel, image: windSpeedImageView)
        windDirectionImageView.image = UIImage(named: Constants.windDirectionImageName)
        let windDirectionView = DescriptionFormView(label: windDirectionLabel, image: windDirectionImageView)
        let secondEmptyView = DescriptionFormView(label: secondEmptyLabel, image: secondEmptyImageView)

        secondStackView = UIStackView(arrangedSubviews: [emptyView, windSpeedView, windDirectionView, secondEmptyView])
        secondStackView.distribution = .fillEqually
        secondStackView.axis = .horizontal
        secondStackView.spacing = 0
        secondStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(secondStackView)
        view.addSubview(secondDottedLine)
        view.addSubview(shareButton)
    }
    
    private func setupConstraints() {
        weatherIcon.snp.makeConstraints { (make) in
            make.height.width.equalTo(view.snp.height).multipliedBy(0.2)
            make.centerX.equalTo(view)
            make.top.equalToSuperview().inset(100)
        }
        cityLabel.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(30)
            make.centerX.equalToSuperview()
            make.top.equalTo(weatherIcon.snp.bottom).inset(0)
        }
        tempLabel.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(50)
            make.centerX.equalToSuperview()
            make.top.equalTo(cityLabel.snp.bottom)
        }
        dottedLine.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(50)
            make.centerX.equalToSuperview()
            make.top.equalTo(tempLabel.snp.bottom)
        }
        firstStackView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalTo(60)
            make.top.equalTo(dottedLine.snp.bottom)
        }
        secondStackView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalTo(60)
            make.top.equalTo(firstStackView.snp.bottom)
        }
        secondDottedLine.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(50)
            make.centerX.equalToSuperview()
            make.top.equalTo(secondStackView.snp.bottom)
        }
        shareButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(30)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(tabBarHeight + 20)
        }
    }
}

// MARK: - Extension CitiesTableViewControllerDelegate

extension CurrentWeatherViewController: CitiesTableViewControllerDelegate {
    func updateCity(lat: Double, lon: Double) {
        viewModel?.updateCity(lat: lat, lon: lon)
        self.delegate?.updateCity(lat: lat, lon: lon)
    }
}
