import UIKit

// MARK: - Protocols

protocol CitiesTableViewControllerDelegate: AnyObject {
    func updateCity(lat: Double, lon: Double)
}

class CitiesTableViewController: UITableViewController {
    
    // MARK: - Public Properties

    var cityResult = [CityModel]()
    var searchResults = [CityModel]()
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    weak var delegate: CitiesTableViewControllerDelegate?
    let searchController = UISearchController(searchResultsController: nil)
    var viewModel: CitiesTableViewViewModel?
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = true
        
        DispatchQueue.global().async {
            self.viewModel = CitiesTableViewViewModel()
        }
        
        tableView.separatorStyle = .none
        searchController.searchBar.becomeFirstResponder()
        tableView.delegate = self
        tableView.tableHeaderView?.backgroundColor = .white
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.titleView = self.searchController.searchBar
        searchController.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
        tableView.register(CitiesTableViewCell.self, forCellReuseIdentifier: "customCell")

    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(isFiltering) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as? CitiesTableViewCell else {
            return UITableViewCell()
        }
        let city = viewModel?.setCell(indexPath, isFiltering)
        cell.isFiltering = isFiltering
        cell.city = city ?? CityModel()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
        let lat = viewModel?.getCoorsinates(indexPath: indexPath, isFiltering: isFiltering).0 ?? 0.0
        let lon = viewModel?.getCoorsinates(indexPath: indexPath, isFiltering: isFiltering).1 ?? 0.0
        self.delegate?.updateCity(lat: lat, lon: lon)
    }
}

// MARK: - Extension UISearchResultsUpdating

extension CitiesTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    private func filterContentForSearchText(_ searchText: String) {
        viewModel?.filterContent(searchText)
        tableView.reloadData()
    }
}
