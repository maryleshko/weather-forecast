import UIKit

class WeatherForecastTableViewController: UITableViewController {

    // MARK: - Private Properties

    private let indicator : UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.startAnimating()
        let transfrom = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
        indicator.transform = transfrom
        indicator.color = .gray
        return indicator
    }()

    // MARK: - Public Properties

    var viewModel: WeatherTableViewViewModel?
    var arrayForPresenting = [WeekdaysArrayModel()] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.indicator.removeFromSuperview()
            }
        }
    }
    var currentWeather = WeatherInfo()

    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.separatorStyle = .none
        addSubviews()
        setupConstraints()

        viewModel = WeatherTableViewViewModel()
        viewModel?.getCoordinates()
        bindOutlets()

        view.backgroundColor = .clear
        title = Constants.forecastItemTitle

        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "cell")
 
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return arrayForPresenting.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayForPresenting[section].weather?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var name = String()
        if section == 0, currentWeather.day == arrayForPresenting[0].weekDay {
            name = "TODAY"
        } else {
            name = arrayForPresenting[section].weekDay?.uppercased() ?? ""
        }
        return name
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: Constants.sectionHeight))
        headerView.backgroundColor = .white
        
        let label = UILabel()
        label.frame = CGRect.init(x: 30, y: 5, width: headerView.frame.width, height: headerView.frame.height)
        var name = String()
        if section == 0, currentWeather.day == arrayForPresenting[0].weekDay {
            name = "TODAY"
        } else {
            name = arrayForPresenting[section].weekDay?.uppercased() ?? ""
        }
        label.text = name
        label.font = .boldSystemFont(ofSize: 20)
        headerView.addSubview(label)

        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.sectionHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomTableViewCell else
        { return UITableViewCell() }
        
        let section = arrayForPresenting[indexPath.section]
        let weather = section.weather?[indexPath.row]
        cell.weather = weather ?? ThreeHourWeather()

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: - Private functions

    private func bindOutlets() {
        self.viewModel?.weatherArrayForPresenting.bind({ (value) in
            self.arrayForPresenting = value
        })
        self.viewModel?.currentWeather.bind({ (value) in
            self.currentWeather = value
        })
    }

    private func addSubviews() {
        indicator.frame = self.view.frame
        self.view.addSubview(indicator)
    }
    private func setupConstraints() {
        indicator.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
}

// MARK: - Extension CurrentWeatherViewControllerProtocol

extension WeatherForecastTableViewController: CurrentWeatherViewControllerProtocol {
    func updateForecast(lat: Double, lon: Double) {
        viewModel?.updateCity(lat: lat, lon: lon)
    }

    func updateCity(lat: Double, lon: Double) {
        viewModel?.updateCity(lat: lat, lon: lon)
    }
}
