import UIKit

class TabBarViewController: UITabBarController {
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        tabBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        let weatherViewController = CurrentWeatherViewController()
        let navigationVC = UINavigationController(rootViewController: weatherViewController)
        navigationVC.tabBarItem.image = #imageLiteral(resourceName: "01d")
        navigationVC.tabBarItem.title = Constants.todayItemTitle
        weatherViewController.navigationItem.title = Constants.todayItemTitle
        
        let forecastViewController = WeatherForecastTableViewController()
        weatherViewController.delegate = forecastViewController
        let secondNavigationVC = UINavigationController(rootViewController: forecastViewController)
        secondNavigationVC.tabBarItem.image = #imageLiteral(resourceName: "10d")
        secondNavigationVC.tabBarItem.title = Constants.forecastItemTitle
        
        viewControllers = [
        navigationVC,
        secondNavigationVC
        ]
        
    }

}
