import UIKit
import SnapKit

class CitiesTableViewCell: UITableViewCell {

    var isFiltering = Bool()
    var city : CityModel? {
        didSet {
            if isFiltering == true {
                nameLabel.text = city?.name
                countryLabel.text = city?.country
            }
        }
    }

    private let nameLabel : UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 25.0)
        return label
    }()
    private let countryLabel : UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15.0)
        return label
    }()
    
    override init (style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(nameLabel)
        addSubview(countryLabel)
        
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    private func setupConstraints() {
        nameLabel.snp.makeConstraints { (make) in
            make.width.equalTo(300)
            make.height.equalTo(40)
            make.leading.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(10)
        }

        countryLabel.snp.makeConstraints { (make) in
            make.width.equalTo(300)
            make.top.equalTo(nameLabel.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(5)
        }

    }

}
