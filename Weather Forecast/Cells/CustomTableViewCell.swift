import UIKit
import SnapKit

class CustomTableViewCell: UITableViewCell {

    var weather : ThreeHourWeather? {
        didSet {
            iconImageView.image = UIImage(named: weather?.icon ?? "")
            descriptionLabel.text = "\(weather?.time ?? "")\n\(weather?.descr ?? "")"
            tempLabel.text = String(Int(weather?.temp?.rounded() ?? 0)) + "°"
        }
    }
    private let iconImageView : UIImageView = {
        let icon = UIImageView()
        icon.clipsToBounds = true
        return icon
    }()
    private let descriptionLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textColor = .black
        return label
    }()
    private let tempLabel : UILabel = {
        let label = UILabel()
        label.textColor = .systemBlue
        label.font = .systemFont(ofSize: 50.0)
        label.textAlignment = .center
        return label
    }()
 
    override init (style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(iconImageView)
        addSubview(descriptionLabel)
        addSubview(tempLabel)
        
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    private func setupConstraints() {

        iconImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalTo(100)
        }

        tempLabel.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalTo(100)
        }
        descriptionLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconImageView.snp.trailing).offset(20)
            make.trailing.equalTo(tempLabel.snp.leading).offset(20)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()

        }

    }
    
    
}
